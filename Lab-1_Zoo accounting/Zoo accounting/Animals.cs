﻿using System;
using System.Collections.Generic;

namespace Zoo_accounting
{
    internal class Animals
    {
        private Database _database;
        private Animal_feed _animalFeed;

        public Animals(Database database, Animal_feed animalFeed)
        {
            _database = database;
            _animalFeed = animalFeed;
        }

        public void DisplayAnimalInfo(int animalIndex)
        {
            if (animalIndex < 0 || animalIndex >= _database.Animals.Count)
            {
                Console.WriteLine("Невірний індекс тварини");
                return;
            }

            string animalName = _database.Animals[animalIndex];
            List<string> foodList = _animalFeed.Foods.ContainsKey(animalName) ? _animalFeed.Foods[animalName] : new List<string> { "Невідомо" };

            Console.WriteLine("Тварина: " + animalName +
                              "\nЇжа:");

            foreach (var food in foodList)
            {
                Console.WriteLine("- " + food);
            }

            Console.WriteLine("\nНазва Вольєр: " + _database.Valiers[animalIndex]);
        }
    }
}

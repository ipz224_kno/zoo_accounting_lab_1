﻿using System;
using System.Collections.Generic;

namespace Zoo_accounting
{
    internal class Database
    {
        public List<string> Animals { get; private set; }
        public int Employees { get; private set; }
        public List<string> NameEmployees { get; private set; }
        public List<string> Valiers { get; private set; }

        public Database()
        {
            Animals = new List<string>() { "Норка європейська", "Ховрах малий", "Кабан дикий", "Жираф", "Видра річкова", "Шуліка рудий" };
            NameEmployees = new List<string>() { "Олег", "Ігор", "Андрій", "Ольга" };
            Valiers = new List<string>() { "Савана", "Тропічний ліс", "Бамбуковий гай", "Екзотичний ліс", "Гірський рельєф", "Пустеля" };
            Employees = NameEmployees.Count;
        }

        public void ListAnimals()
        {
            Console.WriteLine("Список тварин в нашому зоопарку");
            for (int i = 0; i < Animals.Count; i++)
            {
                Console.WriteLine((i + 1) + " " + Animals[i]);
            }
            Console.WriteLine("\n");
        }

        public void NumberOfEmployees()
        {
            Console.WriteLine("Кількість співробітників: " + Employees + "\n");
            for (int i = 0; i < NameEmployees.Count; i++)
            {
                Console.WriteLine((i + 1) + " " + NameEmployees[i]);
            }
            Console.WriteLine("\n");
        }
    }
}

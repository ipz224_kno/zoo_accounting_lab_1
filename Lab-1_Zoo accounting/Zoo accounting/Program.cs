﻿using System;

namespace Zoo_accounting
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Database database = new Database();
            Information information = new Information(database);

            Animal_feed animalFeed = new Animal_feed();
            Animals animals = new Animals(database, animalFeed); 

            bool isRunning = true;

            while (isRunning)
            {
                Console.WriteLine("Зоопарк");
                Console.WriteLine("Меню");
                Console.WriteLine("1 - Інформація зоопарка\n2 - Інформація про тварин\n3 - Корм для тварин\n0 - Вихід");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            ShowZooInformation(database);
                            break;

                        case 2:
                            ShowAnimalInformation(information, animals);
                            break;

                        case 3:
                            ShowVariousMenu(animalFeed);
                            break;

                        case 0:
                            isRunning = false;
                            Console.WriteLine("Робота завершена");
                            break;

                        default:
                            Console.WriteLine("Ви ввели невірне число!");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Введіть коректне число!");
                }
            }
        }

        static void ShowZooInformation(Database database)
        {
            bool isRunning = true;

            while (isRunning)
            {
                Console.WriteLine("1 - Список тварин\n2 - Список співробітників\n0 - Назад");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            database.ListAnimals();
                            break;

                        case 2:
                            database.NumberOfEmployees();
                            break;

                        case 0:
                            isRunning = false;
                            break;

                        default:
                            Console.WriteLine("Ви ввели невірне число!");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Введіть коректне число!");
                }
            }
        }

        static void ShowAnimalInformation(Information information, Animals animals)
        {
            Console.WriteLine("Виберіть тварину");
            information.DisplayAnimals();
            if (int.TryParse(Console.ReadLine(), out int choice))
            {
                animals.DisplayAnimalInfo(choice - 1);
            }
            else
            {
                Console.WriteLine("Введіть коректне число!");
            }
        }

        static void ShowVariousMenu(Animal_feed animalFeed)
        {
            bool isRunning = true;

            while (isRunning)
            {
                Console.WriteLine("1 - Типи живлення тварин\n0 - Назад");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            animalFeed.DisplayAmountOfFood();
                            break;

                        case 0:
                            isRunning = false;
                            break;

                        default:
                            Console.WriteLine("Ви ввели невірне число!");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Введіть коректне число!");
                }
            }
        }
    }
}

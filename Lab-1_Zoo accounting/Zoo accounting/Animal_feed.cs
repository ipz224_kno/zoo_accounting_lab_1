﻿using System;
using System.Collections.Generic;

namespace Zoo_accounting
{
    internal class Animal_feed
    {
        public Dictionary<string, List<string>> Foods { get; private set; }

        public Animal_feed()
        {
            Foods = new Dictionary<string, List<string>>
            {
                { "Травоїдні", new List<string>{"Трава", "Листя"} },
                { "Хижаки", new List<string>{"М'ясо", "Риба"} },
                { "Всеїдні", new List<string>{"М'ясо", "Трава", "Фрукти"} }
            };
        }

        public void DisplayAmountOfFood()
        {
            foreach (var foodGroup in Foods)
            {
                Console.WriteLine(foodGroup.Key + ":");
                foreach (var food in foodGroup.Value)
                {
                    Console.WriteLine("- " + food);
                }
            }
        }
    }
}

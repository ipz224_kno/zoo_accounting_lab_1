﻿using System;
using System.Collections.Generic;

namespace Zoo_accounting
{
    internal class Information
    {
        private readonly Database _database;

        public Information(Database database)
        {
            _database = database;
        }

        public void DisplayAnimals()
        {
            var animalsList = _database.Animals;

            for (int i = 0; i < animalsList.Count; i++)
            {
                Console.WriteLine($"{i + 1} {animalsList[i]}");
            }
        }
    }
}

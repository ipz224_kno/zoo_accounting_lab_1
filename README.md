# lab-1_Zoo

## DRY (Don't Repeat Yourself)

Цей принцип закликає уникати дублювання коду. Кожен шматок інформації має бути представлений в системі лише один раз. Це спрощує зміни та утримання коду.


Ми дотримуємося принципу DRY, використовуючи методи для виконання повторюваних завдань. Наприклад, в класі `Database`, метод `ListAnimals` використовується для виведення списку тварин (див. `Database.cs`, рядки 21-29).

```
public void ListAnimals()
        {
            Console.WriteLine("Список тварин в нашому зоопарку");
            for (int i = 0; i < Animals.Count; i++)
            {
                Console.WriteLine((i + 1) + " " + Animals[i]);
            }
            Console.WriteLine("\n");
        }


```
## KISS (Keep It Simple, Stupid)

Ми дотримуємося принципу KISS, розбиваючи код на прості, зрозумілі методи та класи. Наприклад, клас `Database` містить прості методи для виведення списку тварин та співробітників (див. `Database.cs`).

Одним з прикладів є клас Database. Цей клас має одну відповідальність - зберігати дані про зоопарк. Він виконує це завдання за допомогою методів ListAnimals та NumberOfEmployees, які відповідно виводять список тварин та кількість співробітників.

```
namespace Zoo_accounting
{
    internal class Database
    {
        // ...

        public void ListAnimals()
        {
            Console.WriteLine("Список тварин в нашому зоопарку");
            for (int i = 0; i < Animals.Count; i++)
            {
                Console.WriteLine((i + 1) + " " + Animals[i]);
            }
            Console.WriteLine("\n");
        }

        public void NumberOfEmployees()
        {
            Console.WriteLine("Кількість співробітників: " + Employees + "\n");
            for (int i = 0; i < NameEmployees.Count; i++)
            {
                Console.WriteLine((i + 1) + " " + NameEmployees[i]);
            }
            Console.WriteLine("\n");
        }

    }
}

```

## SOLID

SOLID — це абревіатура, яка об'єднує п'ять основних принципів об'єктно-орієнтованого програмування та проєктування:

### Single Responsibility Principle

Кожен клас в нашому проекті має одну відповідальність. Наприклад, клас `Database` відповідає за зберігання даних про зоопарк (див. `Database.cs`).
```
namespace Zoo_accounting
{
    internal class Database
    {
        // ...

        public void ListAnimals()
        {
            // Виводить список тварин
        }

        public void NumberOfEmployees()
        {
            // Виводить кількість співробітників
        }
    }
}
```

### Open/Closed Principle

Наші класи відкриті для розширення, але закриті для модифікації. Наприклад, ми можемо додати нові види тварин до класу `Database`, не змінюючи його код (див. `Database.cs`).
```
namespace Zoo_accounting
{
    internal class Database
    {
        // ...

        public void AddAnimal(string animal)
        {
            // Додаємо нову тварину до списку
            Animals.Add(animal);
        }

        public void ListAnimals()
        {
            // Виводимо список тварин
        }

        public void NumberOfEmployees()
        {
            // Виводимо кількість співробітників
        }
    }
}
```

## YAGNI (You Aren't Gonna Need It)

Ми дотримуємося принципу YAGNI, додаючи функціональність тільки тоді, коли це необхідно. Наприклад, ми додали клас `Animal_feed` тільки тоді, коли нам знадобилася інформація про їжу для тварин (див. `Animal_feed.cs`).
```
 public void DisplayAmountOfFood()
        {
            foreach (var foodGroup in Foods)
            {
                Console.WriteLine(foodGroup.Key + ":");
                foreach (var food in foodGroup.Value)
                {
                    Console.WriteLine("- " + food);
                }
            }
        }
```
## Composition Over Inheritance

Ми використовуємо композицію замість успадкування, створюючи об'єкти класів `Database` та `Animal_feed` в класі `Information` (див. `Information.cs`, рядки 6-7).
```
internal class Information
    {
        Database database = new Database();
        Animal_feed animal_feed = new Animal_feed();

        // ...
    }

```

## Fail Fast

Ми використовуємо `int.TryParse` для перевірки введення користувача та швидкого виявлення помилок (див. `Program.cs`, рядки 23, 63 та 95,112).
```
if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            ShowZooInformation(database);
                            break;

                        case 2:
                            ShowAnimalInformation(information, animals);
                            break;

                        case 3:
                            ShowVariousMenu(animalFeed);
                            break;

                        case 0:
                            isRunning = false;
                            Console.WriteLine("Робота завершена");
                            break;

                        default:
                            Console.WriteLine("Ви ввели невірне число!");
                            break;
                    }
                }
```
```
 if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            database.ListAnimals();
                            break;

                        case 2:
                            database.NumberOfEmployees();
                            break;

                        case 0:
                            isRunning = false;
                            break;

                        default:
                            Console.WriteLine("Ви ввели невірне число!");
                            break;
                    }
                    
```
```
if (int.TryParse(Console.ReadLine(), out int choice))
            {
                animals.DisplayAnimalInfo(choice - 1);
            }
            else
            {
                Console.WriteLine("Введіть коректне число!");
            }
        }

```

```
if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            animalFeed.DisplayAmountOfFood();
                            break;

                        case 0:
                            isRunning = false;
                            break;

                        default:
                            Console.WriteLine("Ви ввели невірне число!");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Введіть коректне число!");
                }
            }

```
